/* Generated By:JavaCC: Do not edit this line. ParserConstants.java */

/**
 * Token literal values and constants.
 * Generated by org.javacc.parser.OtherFilesGen#start()
 */
public interface ParserConstants {

  /** End of File. */
  int EOF = 0;
  /** RegularExpression Id. */
  int SELECT = 5;
  /** RegularExpression Id. */
  int WHERE = 6;
  /** RegularExpression Id. */
  int FROM = 7;
  /** RegularExpression Id. */
  int GROUP_BY = 8;
  /** RegularExpression Id. */
  int ORDER_BY = 9;
  /** RegularExpression Id. */
  int ALL = 10;
  /** RegularExpression Id. */
  int LEFT_PARENT = 11;
  /** RegularExpression Id. */
  int RIGHT_PARENT = 12;
  /** RegularExpression Id. */
  int DOT = 13;
  /** RegularExpression Id. */
  int COMMA = 14;
  /** RegularExpression Id. */
  int IGUAL = 15;
  /** RegularExpression Id. */
  int OR = 16;
  /** RegularExpression Id. */
  int AND = 17;
  /** RegularExpression Id. */
  int LESS = 18;
  /** RegularExpression Id. */
  int GREATER = 19;
  /** RegularExpression Id. */
  int NUMBERS = 20;
  /** RegularExpression Id. */
  int LETTERS = 21;
  /** RegularExpression Id. */
  int WORDS = 22;

  /** Lexical state. */
  int DEFAULT = 0;

  /** Literal token values. */
  String[] tokenImage = {
    "<EOF>",
    "\" \"",
    "\"\\r\"",
    "\"\\t\"",
    "\"\\n\"",
    "\"SELECT\"",
    "\"WHERE\"",
    "\"FROM\"",
    "\"GROUP BY\"",
    "\"ORDER BY\"",
    "\"*\"",
    "\"(\"",
    "\")\"",
    "\".\"",
    "\",\"",
    "\"=\"",
    "\"OR\"",
    "\"AND\"",
    "\"<\"",
    "\">\"",
    "<NUMBERS>",
    "<LETTERS>",
    "<WORDS>",
  };

}
